<?php

/**
 * @noinspection PhpMissingStrictTypesDeclarationInspection
 */

use Psr\Log\LogLevel;
use TYPO3\CMS\Core\Log\Writer\DatabaseWriter;
use TYPO3\CMS\Core\Log\Writer\FileWriter;

$GLOBALS['TYPO3_CONF_VARS']['LOG']['CoStack']['LogsDev']['writerConfiguration'][LogLevel::DEBUG] = [
    DatabaseWriter::class => [
        'logTable' => 'tx_logsdev_log',
    ],
    FileWriter::class => [
        'logFileInfix' => 'tx_logsdev_log',
    ],
];
