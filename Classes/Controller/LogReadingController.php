<?php

declare(strict_types=1);

namespace CoStack\Logs\Controller;

use CoStack\Logs\Domain\Model\Filter;
use CoStack\Logs\Factory\LogReaderFactory;
use CoStack\Logs\Log\Reader\ReaderCollection;
use CoStack\Logs\Service\LoggingConfigurationService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;

class LogReadingController extends ActionController
{
    use ModuleTemplate;

    protected LoggingConfigurationService $loggingConfigurationService;
    protected LogReaderFactory $logReaderFactory;

    public function injectLoggingConfigurationService(LoggingConfigurationService $loggingConfigurationService): void
    {
        $this->loggingConfigurationService = $loggingConfigurationService;
    }

    public function injectLogReaderFactory(LogReaderFactory $logReaderFactory): void
    {
        $this->logReaderFactory = $logReaderFactory;
    }

    /**
     * @throws NoSuchArgumentException
     *
     * @noinspection PhpUnused Plugin initialize action called by Extbase
     */
    protected function initializeFilterAction(): void
    {
        if ($this->request->hasArgument('filter')) {
            $filter = $this->request->getArgument('filter');
            $this->getBackendUser()->setAndSaveSessionData('tx_logs_filter', $filter);
        } else {
            $filter = $this->getBackendUser()->getSessionData('tx_logs_filter');
            if (null !== $filter) {
                $this->request = $this->request->withArgument('filter', $filter);
                $this->arguments->getArgument('filter')->getPropertyMappingConfiguration()->allowAllProperties();
            }
        }
        $mappingConfig = $this->arguments['filter']->getPropertyMappingConfiguration();
        $mappingConfig->forProperty('fromTime')->setTypeConverter(new DateTimeConverter());
        $mappingConfig->forProperty('toTime')->setTypeConverter(new DateTimeConverter());
    }

    /**
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("filter")
     *
     * @noinspection PhpUnused Plugin action called by Extbase
     */
    public function filterAction(?Filter $filter = null): ResponseInterface
    {
        $filter = $filter ?? new Filter();
        $writers = $this->loggingConfigurationService->getWriters();
        $filter->setAvailableWriters($writers);

        $readers = [];
        foreach ($filter->getWriters() as $writer) {
            if ($writers->has($writer)) {
                $reader = $this->logReaderFactory->fromWriter($writers->get($writer));
                if (null !== $reader) {
                    $readers[] = $reader;
                }
            }
        }
        $readers = new ReaderCollection($readers);
        $logs = $readers->findByFilter($filter);

        $filter->results = $logs;

        $moduleTemplate = $this->createModuleTemplate();
        $moduleTemplate->assign('filter', $filter);
        return $moduleTemplate->renderResponse();
    }

    protected function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
