<?php

declare(strict_types=1);

namespace CoStack\Logs\Controller;

use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;

/**
 * @property Request $request
 * @property UriBuilder $uriBuilder
 */
trait ModuleTemplate
{
    private ModuleTemplateFactory $moduleTemplateFactory;
    private LanguageService $languageService;
    private PageRenderer $pageRenderer;
    /**
     * @var array<string, non-empty-array<string, string>>
     */
    private array $modules = [
        'LLL:EXT:logs/Resources/Private/Language/locallang.xlf:logs' => [
            'action' => 'filter',
            'controller' => 'LogReading',
        ],
        'LLL:EXT:logs/Resources/Private/Language/locallang.xlf:deprecations' => [
            'action' => 'filter',
            'controller' => 'Deprecation',
        ],
    ];
    private array $javascriptModules = [
        '@co-stack/logs/module.js',
    ];

    /**
     * @noinspection PhpUnused
     */
    public function injectModuleTemplateFactory(ModuleTemplateFactory $moduleTemplateFactory): void
    {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
    }

    /**
     * @noinspection PhpUnused
     */
    public function injectLanguageService(LanguageServiceFactory $languageServiceFactory): void
    {
        $this->languageService = $languageServiceFactory->createFromUserPreferences($this->getBackendUser());
    }

    /**
     * @noinspection PhpUnused
     */
    public function injectPageRenderer(PageRenderer $pageRenderer): void
    {
        $this->pageRenderer = $pageRenderer;
    }

    protected function createModuleTemplate(): \TYPO3\CMS\Backend\Template\ModuleTemplate
    {
        /** @var ExtbaseRequestParameters $extbase */
        $extbase = $this->request->getAttribute('extbase');
        $moduleTemplate = $this->moduleTemplateFactory->create($this->request);

        foreach ($this->javascriptModules as $javascriptModule) {
            $this->pageRenderer->loadJavaScriptModule($javascriptModule);
        }

        $menuRegistry = $moduleTemplate->getDocHeaderComponent()->getMenuRegistry();
        $menu = $menuRegistry->makeMenu();
        $menu->setIdentifier('module_selector');

        foreach ($this->modules as $label => $module) {
            $menuItem = $menu->makeMenuItem();
            $menuItem->setTitle($this->languageService->sL($label) ?: $label);
            $menuItem->setHref($this->uriBuilder->uriFor($module['action'], null, $module['controller']));
            $menuItem->setActive(
                $module['controller'] === $extbase->getControllerName()
                && $module['action'] === $extbase->getControllerActionName(),
            );
            $menu->addMenuItem($menuItem);
        }

        $menuRegistry->addMenu($menu);
        return $moduleTemplate;
    }

    private function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }
}
