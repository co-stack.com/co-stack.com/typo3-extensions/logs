<?php

declare(strict_types=1);

namespace CoStack\Logs\Domain\Collection;

use ArrayIterator;
use CoStack\Logs\Domain\Model\Writer;
use IteratorAggregate;

use function array_key_exists;

class Writers implements IteratorAggregate
{
    /** @var array<Writer> */
    protected array $writers = [];

    public function add(Writer $writerConfiguration): void
    {
        $this->writers[$writerConfiguration->uniqueIdentifier] = $writerConfiguration;
    }

    /** @return array<Writer> */
    public function getWriters(): array
    {
        return $this->writers;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->writers);
    }

    public function has(string $writer): bool
    {
        return array_key_exists($writer, $this->writers);
    }

    public function get(string $writer): ?Writer
    {
        return $this->writers[$writer] ?? null;
    }

    public function getAvailableWriterKeys(): array
    {
        $keys = [];
        foreach ($this->writers as $key => $writer) {
            if (!$writer->isDisabled && !$writer->isDeprecationLogger) {
                $keys[] = $key;
            }
        }
        return $keys;
    }
}
