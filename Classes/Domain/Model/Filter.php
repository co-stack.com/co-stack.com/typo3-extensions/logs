<?php

declare(strict_types=1);

namespace CoStack\Logs\Domain\Model;

use CoStack\Logs\Domain\Collection\Writers;
use DateTimeImmutable;
use Psr\Log\LogLevel;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

use function array_key_exists;

class Filter
{
    public const SORTING_DESC = 'DESC';
    public const SORTING_ASC = 'ASC';
    public array $results = [];
    protected ?Writers $availableWriters = null;
    /** @var array<string> */
    protected array $writers = [];
    protected string $requestId = '';
    protected string $level = LogLevel::NOTICE;
    protected string $message = '';
    protected ?DateTimeImmutable $fromTime = null;
    protected ?DateTimeImmutable $toTime = null;
    protected bool $showData = false;
    protected string $component = '';
    protected bool $fullMessage = true;
    protected int $limit = 150;
    protected string $orderField = Log::FIELD_TIME_MICRO;
    protected string $orderDirection = self::SORTING_DESC;

    /**
     * @noinspection PhpUnused Required by Fluid to get property
     */
    public function getAvailableWriters(): Writers
    {
        return $this->availableWriters;
    }

    public function setAvailableWriters(Writers $availableWriters): void
    {
        $this->availableWriters = $availableWriters;
    }

    public function getAvailableWriterOptions(): array
    {
        $options = [];
        foreach ($this->availableWriters->getWriters() as $writer) {
            if (!$writer->isDisabled && !$writer->isDeprecationLogger) {
                $options[$writer->uniqueIdentifier] = $writer->getHumanReadable();
            }
        }
        return $options;
    }

    /**
     * @noinspection PhpUnused Required by Fluid to get property
     * @return array<string>
     */
    public function getWriters(): array
    {
        return $this->writers ?: $this->availableWriters->getAvailableWriterKeys();
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     * @param array<string> $writers
     */
    public function setWriters(array $writers): void
    {
        $this->writers = $writers;
    }

    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setLevel(string $level): void
    {
        $this->level = $level;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getFromTime(): ?DateTimeImmutable
    {
        return $this->fromTime;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setFromTime(DateTimeImmutable $fromTime = null): void
    {
        $this->fromTime = $fromTime;
    }

    public function getToTime(): ?DateTimeImmutable
    {
        return $this->toTime;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setToTime(DateTimeImmutable $toTime = null): void
    {
        $this->toTime = $toTime;
    }

    public function isShowData(): bool
    {
        return $this->showData;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setShowData(bool $showData): void
    {
        $this->showData = $showData;
    }

    public function getComponent(): string
    {
        return $this->component;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setComponent(string $component): void
    {
        $this->component = $component;
    }

    public function isFullMessage(): bool
    {
        return $this->fullMessage;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setFullMessage(bool $fullMessage): void
    {
        $this->fullMessage = $fullMessage;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function getOrderField(): string
    {
        return $this->orderField;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setOrderField(string $orderField): void
    {
        if (array_key_exists($orderField, $this->getOrderFields())) {
            $this->orderField = $orderField;
        }
    }

    public function getOrderDirection(): string
    {
        return $this->orderDirection;
    }

    /**
     * @noinspection PhpUnused Required by Extbase to set property
     */
    public function setOrderDirection(string $orderDirection): void
    {
        $this->orderDirection = $orderDirection;
    }

    /**
     * @noinspection PhpUnused Partials/Log/Filter.html
     */
    public function getLogLevels(): array
    {
        return [
            LogLevel::EMERGENCY => '0 - ' . LogLevel::EMERGENCY,
            LogLevel::ALERT => '1 - ' . LogLevel::ALERT,
            LogLevel::CRITICAL => '2 - ' . LogLevel::CRITICAL,
            LogLevel::ERROR => '3 - ' . LogLevel::ERROR,
            LogLevel::WARNING => '4 - ' . LogLevel::WARNING,
            LogLevel::NOTICE => '5 - ' . LogLevel::NOTICE,
            LogLevel::INFO => '6 - ' . LogLevel::INFO,
            LogLevel::DEBUG => '7 - ' . LogLevel::DEBUG,
        ];
    }

    public function getOrderFields(): array
    {
        return [
            Log::FIELD_TIME_MICRO => LocalizationUtility::translate('filter.time_micro', 'logs'),
            Log::FIELD_REQUEST_ID => LocalizationUtility::translate('filter.request_id', 'logs'),
            Log::FIELD_COMPONENT => LocalizationUtility::translate('filter.component', 'logs'),
            Log::FIELD_LEVEL => LocalizationUtility::translate('filter.level', 'logs'),
        ];
    }

    /**
     * @noinspection PhpUnused Used in Partials/Log/Filter.html
     */
    public function getOrderDirections(): array
    {
        return [
            static::SORTING_DESC => LocalizationUtility::translate('filter.desc', 'logs'),
            static::SORTING_ASC => LocalizationUtility::translate('filter.asc', 'logs'),
        ];
    }
}
