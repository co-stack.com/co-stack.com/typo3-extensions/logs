<?php

declare(strict_types=1);

namespace CoStack\Logs\Domain\Model;

use TYPO3\CMS\Core\Log\Writer\DatabaseWriter;
use TYPO3\CMS\Core\Log\Writer\FileWriter;

use function array_merge;
use function array_replace;
use function hash;
use function implode;
use function json_encode;

class Writer
{
    protected const WRITER_DEFAULTS = [
        DatabaseWriter::class => [
            'logTable' => 'sys_log',
        ],
        FileWriter::class => [
            'logFile' => '',
            'logFileInfix' => '',
            'defaultLogFileTemplate' => '/log/typo3_%s.log',
        ],
    ];
    public readonly bool $isDeprecationLogger;
    public readonly bool $isDisabled;
    public readonly string $uniqueIdentifier;

    public function __construct(
        public readonly string $class,
        public readonly array $config,
        public readonly array $path,
    ) {
        $this->isDeprecationLogger = FileWriter::class === $this->class
            && 'deprecations' === ($config['logFileInfix'] ?? null);
        $this->isDisabled = $config['disabled'] ?? false;
        $this->uniqueIdentifier = hash(
            'sha1',
            $this->class . json_encode(
                array_merge(
                    $path,
                    array_replace(self::WRITER_DEFAULTS, $config),
                ),
            ),
        );
    }

    public function getHumanReadable(): string
    {
        return implode('.', $this->path) . ': ' . $this->class . ' ' . json_encode($this->config);
    }
}
