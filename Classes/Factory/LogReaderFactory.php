<?php

declare(strict_types=1);

namespace CoStack\Logs\Factory;

use CoStack\Logs\Domain\Model\Writer;
use CoStack\Logs\Log\Reader\DatabaseReader;
use CoStack\Logs\Log\Reader\FileReader;
use CoStack\Logs\Log\Reader\Reader;
use TYPO3\CMS\Core\Log\Writer\DatabaseWriter;
use TYPO3\CMS\Core\Log\Writer\FileWriter;
use TYPO3\CMS\Core\Utility\GeneralUtility;

use function implode;

class LogReaderFactory
{
    protected const MAP = [
        FileWriter::class => FileReader::class,
        DatabaseWriter::class => DatabaseReader::class,
    ];

    public function fromWriter(Writer $writer = null): ?Reader
    {
        if (null === $writer) {
            return null;
        }
        $readerClass = self::MAP[$writer->class] ?? null;
        if ($readerClass) {
            $logWriter = GeneralUtility::makeInstance($writer->class, $writer->config);
            return GeneralUtility::makeInstance($readerClass, $logWriter, implode('.', $writer->path));
        }
        return null;
    }
}
