<?php

declare(strict_types=1);

namespace CoStack\Logs\Log\Reader;

use CoStack\Logs\Domain\Model\Filter;
use CoStack\Logs\Domain\Model\Log;
use DateTimeImmutable;
use DateTimeInterface;
use Generator;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\Writer\FileWriter;
use TYPO3\CMS\Core\Log\Writer\WriterInterface;

use function count;
use function fgets;
use function fopen;
use function json_decode;
use function preg_match;
use function str_contains;
use function strrpos;
use function substr;

use const JSON_THROW_ON_ERROR;

class FileReader implements Reader
{
    protected readonly string $logFile;

    public function __construct(WriterInterface|FileWriter $logWriter, public readonly string $componentPrefix)
    {
        $this->logFile = $logWriter->getLogFile();
    }

    public static function getDefaultConfigForUniqueKeys(): array
    {
        return [
            'logFile' => '',
            'logFileInfix' => '',
            'defaultLogFileTemplate' => '/log/typo3_%s.log',
        ];
    }

    public function findByFilter(Filter $filter): array
    {
        $logs = [];

        $stream = fopen($this->logFile, 'rb');
        foreach ($this->readEntries($stream) as $entry) {
            $rawEntry = $this->parseEntry($entry);
            if (null !== $rawEntry) {
                $log = $this->createLogIfNotFiltered($rawEntry, $filter);
                if (null !== $log) {
                    $logs[] = $log;
                    if (count($logs) >= $filter->getLimit()) {
                        break;
                    }
                }
            }
        }
        return $logs;
    }

    protected function readEntries($stream): Generator
    {
        $entry = '';
        while ($string = fgets($stream)) {
            // As long as there is no new entry beginning with the new
            // line, we collect the lines because they belong together
            // If $entry is empty, we found the first line.
            if (!$this->isNewEntry($string) || '' === $entry) {
                $entry .= $string;
            } else {
                // We found the beginning of a new log entry. Return the
                // lines up to here and begin collecting the next lines.
                yield $entry;
                // Reset the buffer to the beginning of the new entry we just found.
                $entry = $string;
            }
        }
        // After reading the last line of the file we get false and the loop breaks,
        // but we still need to return the lines we've read since the last yield.
        if ('' !== $entry) {
            yield $entry;
        }
    }

    /**
     * @noinspection RegExpRedundantEscape
     */
    protected function isNewEntry(string $string): bool
    {
        return 1 === preg_match(
                '/^\w{3}, \d{2} \w{3} \d{4} \d{2}:\d{2}:\d{2} \+\d{4} \[\w+\] request="\w+" component="[\w\.]+":/',
                $string,
            );
    }

    protected function parseEntry(mixed $entry): ?array
    {
        $matching = preg_match(
            '/^(?P<date>\w{3}, \d{2} \w{3} \d{4} \d{2}:\d{2}:\d{2} \+\d{4}) \[(?P<level>\w+)\] request="(?P<requestId>\w+)" component="(?P<component>[\w\.]+)": (?P<message>.*)/s',
            $entry,
            $matches,
        );
        if (1 !== $matching) {
            return null;
        }
        // Data is optional in log entries.
        // Splitting the message into "message" and "data" is easier and faster than searching "dot star"
        // followed by an optional capturing group (which would require a negative lookahead).
        if (str_contains($matches['message'], ' - {')) {
            $message = $matches['message'];
            $lastPos = strrpos($message, ' - {');
            $matches['message'] = substr($message, 0, $lastPos);
            $matches['data'] = substr($message, $lastPos + 3);
        }

        return $matches;
    }

    protected function createLogIfNotFiltered(array $rawEntry, Filter $filter): ?Log
    {
        if ('' !== $filter->getRequestId()) {
            if (!str_contains($rawEntry['requestId'], $filter->getRequestId())) {
                return null;
            }
        }

        if ('' !== $filter->getComponent()) {
            if (!str_contains($rawEntry['component'], $filter->getComponent())) {
                return null;
            }
        }

        if ('' !== $this->componentPrefix) {
            if (!str_contains($rawEntry['component'], $this->componentPrefix)) {
                return null;
            }
        }

        if ('' !== $filter->getMessage()) {
            if (!str_contains($rawEntry['message'], $filter->getMessage())) {
                return null;
            }
        }

        $normalizeLevel = LogLevel::normalizeLevel($rawEntry['level']);

        if ('' !== $filter->getLevel()) {
            if ($normalizeLevel > LogLevel::normalizeLevel($filter->getLevel())) {
                return null;
            }
        }

        $logDate = DateTimeImmutable::createFromFormat(DateTimeInterface::RFC1123, $rawEntry['date']);

        $timeLimitLow = $filter->getFromTime();
        if (null !== $timeLimitLow && $logDate < $timeLimitLow) {
            return null;
        }

        $timeLimitHigh = $filter->getToTime();
        if (null !== $timeLimitHigh && $logDate > $timeLimitHigh) {
            return null;
        }

        $data = null;
        if ($filter->isShowData()) {
            $data = json_decode($rawEntry['data'] ?? '[]', true, 512, JSON_THROW_ON_ERROR);
        }

        return new Log(
            $rawEntry['requestId'],
            (float) $logDate->format('U.u'),
            $rawEntry['component'],
            $normalizeLevel,
            $rawEntry['message'],
            $data,
        );
    }
}
