<?php

declare(strict_types=1);

namespace CoStack\Logs\Service;

use CoStack\Logs\Domain\Collection\Writers;

use CoStack\Logs\Domain\Model\Writer;

use function array_pop;
use function is_array;

class LoggingConfigurationService
{
    public function getWriters(array $customWriterConfig = null): Writers
    {
        $writerConfig = $customWriterConfig ?? $GLOBALS['TYPO3_CONF_VARS']['LOG'] ?? [];

        $writers = new Writers();

        $this->getWriterFromConfig($writerConfig, $writers);

        return $writers;
    }

    protected function getWriterFromConfig(array $configuration, Writers $writers, array $path = []): void
    {
        foreach ($configuration as $key => $value) {
            if (is_array($value)) {
                if ('processorConfiguration' === $key) {
                    continue;
                }
                if ('writerConfiguration' !== $key) {
                    $path[] = $key;
                    $this->getWriterFromConfig($value, $writers, $path);
                    array_pop($path);
                } else {
                    $this->getWritersForLevel($value, $writers, $path);
                }
            }
        }
    }

    protected function getWritersForLevel(array $value, Writers $writers, array $path = []): void
    {
        foreach ($value as $writersForLevel) {
            if (is_array($writersForLevel)) {
                foreach ($writersForLevel as $writerClass => $writerOptions) {
                    $writers->add(new Writer($writerClass, (array)$writerOptions, $path));
                }
            }
        }
    }
}
