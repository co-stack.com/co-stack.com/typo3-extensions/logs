<?php

declare(strict_types=1);

return [
    'dependencies' => ['backend'],
    'imports' => [
        '@co-stack/logs/' => 'EXT:logs/Resources/Public/JavaScript/',
    ],
];
