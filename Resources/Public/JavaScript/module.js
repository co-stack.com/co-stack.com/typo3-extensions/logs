'use strict';

import DateTimePicker from '@typo3/backend/date-time-picker.js';
import '@typo3/backend/input/clearable.js';

document.querySelectorAll('.module .t3js-clearable').forEach(el => el.clearable());
document.querySelectorAll('.module .t3js-datetimepicker').forEach(el => DateTimePicker.initializeField(el, document.documentElement.lang));
